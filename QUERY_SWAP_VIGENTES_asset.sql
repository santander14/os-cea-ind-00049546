  SELECT   
  HEADER.PK,
  NVL(
TO_NUMBER(PGT_PRG.pkg_pgtutility.f_GetObjSource(9.4, EVENT.PK, 1546.4, 0), 99999999)             ,
TO_NUMBER(PGT_PRG.pkg_pgtutility.f_GetObjSource(513.40, EVENT.PK, 1546.4, 0), 99999999)          )           "MUREX",          
DECODE (ASSETSCHD.FK_COUPONTYPE,268.4, 'ASSET'),
          ASSETSCHD.SEQUENCE,
          ASSETSCHD.STARTDATE,
          ASSETSCHD.ENDDATE,
          ASSETSCHD.PAYMENTDATE,
          ASSETSCHD.NOMINAL,
          ASSETSCHD.INTCALCAMOUNT,
          ASSETSCHD.INTINITIALAMOUNT,
          ASSETSCHD.CASHFLOWAMOUNT,
          ASSETSCHD.COUPONRATE,
          (SELECT DESCRIPTION FROM PGT_SYS.T_PGT_BASIS_S WHERE PK=ASSETSCHD.FK_COUPONRATEBASIS),
          FWD.FIXINGDATE,
          PGT_PRG.pkg_pgtutility.f_GetQuoteRefCode(ASSETSCHD.FK_QUOTEREFERENCE) AS Reference,
          ASSETSCHD.FK_CREDITSPREADTYPE AS Spread,
          CURR.ShortName AS Currency
        --INTO    num_NominalVivoAsset
        FROM    "PGT_TRD".T_PGT_TRADE_EVENTS_S                  EVENT,
                "PGT_TRD".T_PGT_TRADE_HEADER_S                  HEADER,
                "PGT_TRD".T_PGT_CCS_S                           CCS,
                "PGT_TRD".T_PGT_INTRATE_S                       ASSETIR,
                "PGT_TRD".T_PGT_INTRATE_SCHEDULE_S              ASSETSCHD,
                PGT_TRD.T_PGT_FWDSTARTING_S                     FWD,
                PGT_STC.T_PGT_CURRENCY_S                    CURR
                 
        WHERE   EVENT.FK_OWNER_OBJ                              = 1546.4                                   AND
                EVENT.PK                                        = HEADER.FK_PARENT                         AND
                HEADER.FK_OWNER_OBJ                             = 1546.4                                   AND
                HEADER.FK_EXTENSION                             = 11867.4                                  AND
                HEADER.PK                                       = 15472549.25                            AND
                HEADER.PK                                       = CCS.FK_PARENT                            AND
                CCS.FK_OWNER_OBJ                                = 1476.4                                   AND
                CCS.FK_EXTENSION                                = 14962.4                                  AND
                CCS.PK                                          = ASSETIR.FK_PARENT                        AND
                ASSETIR.FK_OWNER_OBJ                            = 1752.4                                   AND
                ASSETIR.FK_EXTENSION                            = 14959.4                                  AND
                ASSETIR.PK                                      = ASSETSCHD.FK_PARENT                      AND
                ASSETSCHD.FK_OWNER_OBJ                          = 12981.4                                  AND
                ASSETSCHD.FK_EXTENSION                          = 43965.4                                  AND
              --  ASSETSCHD.STARTDATE                               < TO_DATE( SYSDATE, 'dd/mm/rrrr' )    AND --AJR Task 24833.7 30-07-2013
              --  ASSETSCHD.ENDDATE                                 >= TO_DATE( SYSDATE, 'dd/mm/rrrr' )   AND
                ASSETSCHD.FK_REVREASON                          IS NULL
                AND FWD.FK_PARENT =     ASSETSCHD.PK        
                AND CURR.PK = CCS.FK_CURRENCY
               

                
               