 SELECT   
          HEADER.PK,
          NVL(
          TO_NUMBER(PGT_PRG.pkg_pgtutility.f_GetObjSource(9.4, EVENT.PK, 1546.4, 0), 99999999)             ,
          TO_NUMBER(PGT_PRG.pkg_pgtutility.f_GetObjSource(513.40, EVENT.PK, 1546.4, 0), 99999999)          )           "MUREX",
          DECODE (LIABSCHD.FK_COUPONTYPE,267.4,'LIAB') ,
          LIABSCHD.SEQUENCE,
          LIABSCHD.STARTDATE,
          LIABSCHD.ENDDATE,
          LIABSCHD.PAYMENTDATE,
          LIABSCHD.NOMINAL,
          LIABSCHD.INTCALCAMOUNT,
          LIABSCHD.INTINITIALAMOUNT,
          LIABSCHD.CASHFLOWAMOUNT,
          LIABSCHD.COUPONRATE,
          (SELECT DESCRIPTION FROM PGT_SYS.T_PGT_BASIS_S WHERE PK=LIABSCHD.FK_COUPONRATEBASIS),
          PGT_PRG.pkg_pgtutility.f_GetQuoteRefCode(LIABSCHD.FK_QUOTEREFERENCE) AS Reference,
          LIABSCHD.FK_CREDITSPREADTYPE AS Spread,
          CURR.ShortName AS Currency,
          
          
          FWD.FIXINGDATE
                 --   INTO    num_NominalVivoLiab
                    FROM    PGT_TRD.T_PGT_TRADE_EVENTS_S                  EVENT,
                            PGT_TRD.T_PGT_TRADE_HEADER_S                  HEADER,
                            PGT_TRD.T_PGT_CCS_S                           CCS,
                            PGT_TRD.T_PGT_INTRATE_S                       LIABIR,
                            PGT_TRD.T_PGT_INTRATE_SCHEDULE_S              LIABSCHD,
                            PGT_TRD.T_PGT_FWDSTARTING_S                   FWD,
                            PGT_STC.T_PGT_CURRENCY_S                      CURR
                    WHERE   EVENT.FK_OWNER_OBJ                              = 1546.4                                   AND
                            EVENT.PK                                        = HEADER.FK_PARENT                         AND
                            HEADER.FK_OWNER_OBJ                             = 1546.4                                   AND
                            HEADER.FK_EXTENSION                             = 11867.4                                  AND
                           -- HEADER.PK                                       = num_HeaderPK                             AND
                            HEADER.PK                                       = CCS.FK_PARENT                            AND
                            CCS.FK_OWNER_OBJ                                = 1476.4                                   AND
                            CCS.FK_EXTENSION                                = 14962.4                                  AND
                            CCS.PK                                          = LIABIR.FK_PARENT                         AND
                            LIABIR.FK_OWNER_OBJ                             = 1752.4                                   AND
                            LIABIR.FK_EXTENSION                             = 14960.4                                  AND
                            LIABIR.PK                                       = LIABSCHD.FK_PARENT                       AND
                            LIABSCHD.FK_OWNER_OBJ                           = 12981.4                                  AND
                            LIABSCHD.FK_EXTENSION                           = 43965.4                                  AND
                          --  LIABSCHD.ENDDATE                                = dat_LiabEnddate                          AND
                            LIABSCHD.FK_REVREASON                           IS NULL  AND 
                            FWD.FK_PARENT(+)  = LIABSCHD.pk AND
                            CURR.PK = CCS.FK_CURRENCY AND 
                            HEADER.PK IN (15472548.25);
                                     
                            
                            /*

15,472,540.25
15,472,541.25
15,472,543.25
15,472,544.25
15,472,546.25
15,472,547.25
15,472,548.25

*/