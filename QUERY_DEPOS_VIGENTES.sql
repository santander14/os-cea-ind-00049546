  SELECT   
        HEADER.PK,
        NVL(
        TO_NUMBER(PGT_PRG.pkg_pgtutility.f_GetObjSource(9.4, EVENT.PK, 1546.4, 0), 99999999)             ,
        TO_NUMBER(PGT_PRG.pkg_pgtutility.f_GetObjSource(513.40, EVENT.PK, 1546.4, 0), 99999999)          )           "MUREX",
        PGT_PRG.pkg_pgtutility.f_GetQuoteRefCode(SCHD.FK_QUOTEREFERENCE) AS Reference,
        'N/A' PATA,
        SCHD.FK_CREDITSPREADTYPE AS Spread,
        SCHD.SEQUENCE,
        SCHD.STARTDATE,
        SCHD.ENDDATE,
        SCHD.PAYMENTDATE,
        SCHD.NOMINAL,
        SCHD.INTCALCAMOUNT,
        SCHD.INTINITIALAMOUNT,
        SCHD.CASHFLOWAMOUNT,
        SCHD.COUPONRATE,
        (SELECT DESCRIPTION FROM PGT_SYS.T_PGT_BASIS_S WHERE PK=SCHD.FK_COUPONRATEBASIS) COUPON_RATE_BASIS,
        FWD.FIXINGDATE,
        CURR.ShortName AS Currency
        FROM    PGT_TRD.T_PGT_TRADE_EVENTS_S                  EVENT,
                PGT_TRD.T_PGT_TRADE_HEADER_S                  HEADER,
                PGT_TRD.T_PGT_MM_S                            DL,  
                PGT_TRD.T_PGT_INTRATE_S                       IR,
                PGT_TRD.T_PGT_INTRATE_SCHEDULE_S              SCHD,
                PGT_TRD.T_PGT_FWDSTARTING_S                   FWD,
                PGT_STC.T_PGT_CURRENCY_S                      CURR
        WHERE   EVENT.FK_OWNER_OBJ                              = 1546.4                              AND
                EVENT.PK                                        = HEADER.FK_PARENT                    AND
                HEADER.FK_OWNER_OBJ                             = 1546.4                              AND
                HEADER.FK_EXTENSION                             = 11867.4                             AND
                HEADER.PK                                       = 7219436.25                        AND
                HEADER.PK                                       = DL.FK_PARENT                        AND
                DL.FK_OWNER_OBJ                                 = 1476.4                               AND
                DL.FK_EXTENSION                                 = 14912.4                              AND
                DL.PK                                           = IR.FK_PARENT                          AND
                IR.FK_OWNER_OBJ                                 = 1686.4                               AND
                IR.FK_EXTENSION                                 = 14415.4                              AND
                IR.PK                                           = SCHD.FK_PARENT                       AND
                SCHD.FK_OWNER_OBJ                               = 12981.4                                  AND
                SCHD.FK_EXTENSION                               = 43965.4                                  AND
            --    SCHD.STARTDATE                               < TO_DATE( SYSDATE, 'dd/mm/rrrr' )    AND --AJR Task 24833.7 30-07-2013
            --    SCHD.ENDDATE                                 >= TO_DATE( SYSDATE, 'dd/mm/rrrr' )   AND 
                SCHD.FK_REVREASON                               IS NULL
                AND  FWD.FK_PARENT  (+) = SCHD.PK               AND
                FWD.FK_EXTENSION  (+) =  43887.4                AND
                FWD.FK_OWNER_OBJ (+) = 12979.4                  AND
                CURR.PK = IR.FK_CURRENCY

                


      
                    
           

                
               

/*
SELECT * FROM all_constraints where r_constraint_name in (select constraint_name from all_constraints where table_name='T_PGT_INTRATE_S');
*/